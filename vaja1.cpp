#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

using namespace std;

bool isValid(string seq)
{
    int l = seq.length(), ll = 0;
    int index = l - 2;
    int cond = ceil(seq.length() / 2.0) - 1;
    bool ok = true;
    while (index >= cond)
    {
        ok = true;
        ll = l - index - 1;
        // Check if substring are equal but start checking at the back
        for (int i = 0; i < ll; ++i)
        {
            if (seq[l - 1 - i] != seq[index - i])
            {
                ok = false;
                break;
            }
        }
        if (ok)
            return false;
        index--;
    }
    return true;
}

int main(int argc, char *argv[])
{
    bool ok = true;
    string seq = "A";
    int counter = 1, i = 65;
    int n, c;

    ifstream input(argv[1]);
    input >> n >> c;
    input.close();

    c += 65;

    // Till we find the n-th one
    while (counter < n)
    {
        if (!ok)
        {
            i = (int)seq[seq.length() - 1] + 1;
            seq.pop_back();
        }
        else
        {
            i = 65;
        }
        ok = true;
        // We go over all available chars
        while (i < c)
        {
            if (isValid(seq + char(i)))
            {
                counter++;
                seq.push_back(char(i));
                break;
            }
            i++;
        }
        if (i == c)
            ok = false;
    }
    cout << seq << endl;
    return 0;
}