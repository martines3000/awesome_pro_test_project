import subprocess

passed = ""

for i in range(4):
    result = subprocess.run(
        ["./build/vaja1.exe", f"./tests/testni_primer{i+1}.txt"], capture_output=True, text=True
    ).stdout

    solution = open(f"./tests/solution{i+1}.txt").read()

    if(solution == result):
        passed += "."
    else:
        passed += "F"

print(passed)

if("F" in passed):
    print("1 or more tests failed!")
    exit(1)
else:
    print("All tests passed successfully!")
    exit(0)
